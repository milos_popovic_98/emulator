#include <iostream>
#include <vector>
#include <regex>
#include <fstream>
#include <map>
#include "Linker.h"
#include "Emulator.h"
using namespace std;

int main(int argc,char* argv[]) {
	map<string, int> sections;
	vector<string> files;
	regex placeArg("^-place=[a-z,A-Z,0-9,_]+@[0-9]+$");
	for (int i = 1; i < argc; i++) {
		if (regex_match(argv[i], placeArg)) {
			string str(argv[i]);
			str = str.substr(7);
			int p = str.find('@');
			string name = str.substr(0, p);
			str = str.substr(p+1);
			p = stoi(str);
			sections.insert(pair<string, int>(name, p));
		}
		else {
			string str(argv[i]);
			files.push_back(str);
		}
	}
	
	Linker linker;
	linker.process(files, sections);
 

	Emulator emulator(linker.getMemory(), linker.getMain());
	emulator.execute();

	
	return 0;/*
	ifstream f1, f2;
	f1.open("prekidi.dat", ios::binary | ios::in);
	f2.open("timer.dat", ios::binary | ios::in);
	vector<ifstream*> files;
	files.push_back(&f1);
	files.push_back(&f2);
	map<string, int> sections;
	sections.insert(pair<string, int>("text", 400));
	Linker linker;
	linker.process(files, sections);
	f1.close();
	f2.close();
	return 0;*/
}