#include "Keylistener.h"
#include <poll.h>
#include <unistd.h>
#include <thread>

static int KeyEntry = 3;

Keylistener::Keylistener(Emulator* em)
{
	this->em = em;
	this->end = false;
}

void Keylistener::run()
{
	char* c;
	struct pollfd input[1] = { {fd: 0, events : POLLIN}; };
	while (!end) {
		int ret_poll = poll(input, 1, 1000);
		if (ret_poll > 0) {
			read(0, c, 1);
			em->dataIn(*c);
			em->putInterruptReq(KeyEntry);
		}
	}
}

void Keylistener::start() {
	nit = new thread(&Keylistener::run, this);
}
