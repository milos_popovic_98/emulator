#include "Timer.h"
#include <thread>
#include <chrono>
using namespace std::this_thread;
using namespace std;

static int TimerEntry = 2;

Timer::Timer(Emulator* em, int period)
{
	this->em = em;
	this->end = false;
	this->period = period;
}

void Timer::start()
{
	nit = new thread(&Timer::run, this);
}



void Timer::run()
{
	while (!end) {
		sleep_for(std::chrono::milliseconds(period));
		em->putInterruptReq(TimerEntry);
	}
}

void Timer::setEnd()
{
	this->end = true;
}

void Timer::setPeriod(int val)
{
	switch (val) {
	case 0: case 1: case 2: case 3: 
		period = (val+1)*500;
		break;
	case 4:
		period = 5000;
		break;
	case 5:
		period = 10000;
		break;
	case 6:
		period = 30000;
		break;
	case 7:
		period = 60000;
		break;
	}
}
