#include "Linker.h"
#include <iterator>
#include <iostream>
int Linker::N = 65536;




void Linker::resolveUnd() {
	map<string, Symbol*> symbols;
	map<string, Symbol*>::iterator it1, it2;
	for (size_t i = 0; i < modules.size(); i++) {
		symbols = modules[i].symbols;
		for (it1 = symbols.begin(); it1 != symbols.end(); it1++) {
			Symbol* sym = it1->second;
			if (sym->sectionNum == 0) {
				it2 = definedSyms.find(sym->name);
				if (it2 == definedSyms.end()) {
					cout << "Simbol nije definisan " << sym->name << endl;
					exit(-1);
				}
				sym->val = it2->second->val;
			}
		}
	}
	
}
void Linker::resolveRelocs()
{
	for (size_t i = 0; i < modules.size(); i++) {
		modules[i].resolveRelocs(memory);
	}
}
void Linker::process(vector<string> f,map<string,int> positions)
{
	map<string, Symbol>::iterator iter;
	for (size_t i = 0; i < f.size(); i++) {
		modules.push_back(Module());
		modules[i].processModule(f[i]);
	}

	map<string, int>::iterator it;
	map<string, Section*>::iterator sec, tmp, t1;
	map<string, Section*> m, m1;
	int pos = 1000;
	char* moduleMem;
	for (it = positions.begin(); it != positions.end(); it++) {
		string name = it->first;
		pos = it->second;
		for (size_t j = 0; j < modules.size(); j++) {
			m = modules[j].sections;
			sec = m.find(name);
			if (sec != m.end()) {
				moduleMem = modules[j].memTemp;
				memcpy(&(memory[pos]), &(moduleMem[sec->second->offset]), sec->second->size);
				sec->second->val = pos;
				pos += sec->second->size;
			}
		}
	}
	
	for (size_t i = 0; i < modules.size(); i++) {
		m = modules[i].sections;
		for (t1 = m.begin(); t1 != m.end(); t1++) {
			Section* s = t1->second;
			string first = t1->first;
			if (s->val != -1) continue;
			moduleMem = modules[i].memTemp;
			memcpy(&(memory[pos]), &(moduleMem[s->offset]), s->size);
			s->val = pos;
			pos += s->size;
			for (size_t j = i + 1; j < modules.size(); j++) {
				m1 = modules[j].sections;
				tmp = m1.find(first);
				if (tmp != m1.end()) {
					s = tmp->second;
					moduleMem = modules[j].memTemp;
					memcpy(&(memory[pos]), &(moduleMem[s->offset]), s->size);
					s->val = pos;
					pos += s->size;
				}
			}
		}
		modules[i].updateSymbols(this);
	}
	resolveUnd();
	resolveRelocs();
	memory[0xFF10] = 0x0;
}

char* Linker::getMemory() const
{
	return this->memory;
}

int Linker::getMain() const
{
	return _main;
}




void Module::processModule(string str)
{
	ifstream file;
	file.open(str, ios::binary | ios::in);
	int lengthOfSections = 0;
	int numOfSections,numOfSymbols,num,size,len,val,sectionNum;
	char* temp;
	string name;
	file.read((char*)&numOfSections, sizeof(int));
	for (int i = 0; i < numOfSections; i++) {
		file.read((char*)&len, sizeof(int));
		temp = new char[len + 1];
		file.read(temp, len);
		temp[len] = '\0';
		name = temp;
		file.read((char*)&num, sizeof(int));
		file.read((char*)&size, sizeof(int));
		names.push_back(name);
		sections.insert(pair<string,Section*>(name,new Section(name, num, size,lengthOfSections)));
		lengthOfSections += size;
		delete[] temp;
	}
	file.read((char*)&numOfSymbols, sizeof(int));
	for (int i = 0; i < numOfSymbols; i++) {
		file.read((char*)&len, sizeof(int));
		temp = new char[len + 1];
		file.read(temp, len);
		temp[len] = '\0';
		name = temp;
		file.read((char*)&sectionNum, sizeof(int));
		file.read((char*)&num, sizeof(int));
		file.read((char*)&val, sizeof(int));
		symNames.insert(pair<int, string>(num, name));
		symbols.insert(pair<string,Symbol*>(name,new Symbol(name, sectionNum, num, val)));
	}

	for (int i = 0; i < numOfSections; i++) {
		int numOfRels;
		file.read((char*)&numOfRels, sizeof(int));
		int offset, type, symbol;
		for (int j = 0; j < numOfRels; j++) {
			file.read((char*)&offset, sizeof(int));
			file.read((char*)&type, sizeof(int));
			file.read((char*)&symbol, sizeof(int));
			relocations.push_back(new Relocation(offset, i, symbol, type));
		}
	}
	
	memTemp = new char[lengthOfSections];
	file.read(memTemp, lengthOfSections);
	file.close();
}

void Module::updateSymbols(Linker* linker)
{
	map<string, Symbol*>::iterator iter1, iter3;
	map<string, Section*>::iterator iter2;
	for (iter1 = symbols.begin(); iter1 != symbols.end(); iter1++) {
		Symbol* sym = iter1->second;
		if (sym->sectionNum != 0) {
			string sectionName = names[sym->sectionNum-1];
			iter2 = sections.find(sectionName);
			sym->val += iter2->second->val;
			iter3 = linker->definedSyms.find(sym->name);
			if (iter3 != linker->definedSyms.end()) {
				std::cout << "Visestruko definisan simbol " << sym->name << endl;
				exit(-1);
			}
			linker->definedSyms.insert(pair<string, Symbol*>(sym->name, sym));
			if (sym->name == "main") {
				linker->_main = sym->val;
			}
		}
	}
}

void Module::resolveRelocs(byte*& memory)
{
	map<string, Section*>::iterator iter;
	map<string, Symbol*>::iterator iter1;
	int16_t val;
	int16_t offset;
	int16_t* pos;

	for (size_t i = 0; i < relocations.size(); i++) {
		Relocation* r = relocations[i];
		if ((unsigned)r->symbol <= names.size()) {
			iter = sections.find(names[r->symbol - 1]);
			val = iter->second->val;
		}
		else {
			string symName = symNames.find(r->symbol)->second;
			iter1 = symbols.find(symName);
			val = (int16_t)iter1->second->val;
		}
		iter = sections.find(names[r->section]);
		offset = (int16_t)(r->offset + iter->second->val);
		if (r->type) {
			val -= offset;
		}
		pos = (int16_t*)(&memory[offset]);
		*pos = *pos + val;
	}
}

Section::Section(string name, int num, int size, int offset)
{
	this->name = name;
	this->num = num;
	this->size = size;
	this->offset = offset;
}

Symbol::Symbol(string name, int sectionNum, int num, int val)
{
	this->name = name;
	this->sectionNum = sectionNum;
	this->num = num;
	this->val = val;
}

Relocation::Relocation(int offset, int section, int sym, int type)
{
	this->offset = offset;
	this->section = section;
	this->symbol = sym;
	this->type = type;
}
