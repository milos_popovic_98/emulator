#ifndef LINKER_H_
#define LINKER_H_
#include <vector>
#include <fstream>
#include <map>

using namespace std;

typedef char byte;

struct Section {
	string name;
	int num;
	int size;
	int offset;
	int val = -1;
	Section(string name, int num, int size, int offset);
};
struct Symbol {
	string name;
	int sectionNum;
	int num;
	int val;
	Symbol(string name, int sectionNum, int num, int val);
};
struct Relocation {
	int offset;
	int section;
	int symbol;
	int type;
	Relocation(int offset, int section, int symbol, int type);
};
class Module;
class Linker
{
public:
		void process(vector<string> f, map<string,int>positions);
		byte* getMemory() const;
		int getMain() const;
protected:
	void resolveUnd();
	void resolveRelocs();
private:
	vector<Module> modules;
	map<string, Symbol*> definedSyms;
	static int N;
	byte* memory = new byte[N];
	friend class Module;
	int _main = -1;
};
class Module {
public:
	void processModule(string str);
	void updateSymbols(Linker* linker);
	void resolveRelocs(byte*& memory);
private:
	byte* memTemp;
	map<string,Section*> sections;
	map<string,Symbol*> symbols;
	vector<Relocation*> relocations;
	vector<string> names;
	map<int, string> symNames;
	friend class Linker;
};




#endif
