#include "Emulator.h"
#include "Keylistener.h"
#include "Timer.h"
#include <iostream>

using namespace std;

static int NUM_OF_INSTRUCTIONS = 25;
static uint16_t data_in = 0xFF02;

int period = -1;
enum Instructions {
	halt,iret,ret,_int,call,jmp,jeq,jne,jgt,push,pop,xchg,mov,add,sub,mul,_div,cmp, _not, _and, _or, _xor, test, shl, shr
};
enum AddrType {
	imm,regdir,regind,offset,mem
};
enum Flags {
	Z=1,O=2,C=4,N=8,Tr=1<<13,Tl=1<<14,I=1<<15
};
Emulator::Emulator(char* memory, int _main):memory(memory)
{   
	if (_main == -1) {
		cout << "Nije definisan simbol main" << endl;
		exit(-1);
	}
	this->initialization = true;
	this->_main = _main;
	this->psw = 0;
	this->sp = 0xFF00;
	for (int i = 0; i < 6; i++) {
		registers[i] = 0;
	}
	timer = new Timer(this, 500);
	keylistener = new Keylistener(this);
}



void Emulator::getInstrDescr(uint8_t& instr) {
	instr = *(int*)(&memory[pc]);
	pc++;
}
inline void Emulator::execIret()
{
	if (initialization) {
		this->pc = _main;
		keylistener->start();
		timer->start();
		initialization = false;
    }
	else {
		psw = *(int16_t*)(&memory[sp]);
		sp += 2;
		execRet();
		if (terminal) {
			full.unlock();
			terminal = false;
		}
	}
}
void Emulator::execute()
{
	uint8_t instrDescr, instr;
	bool end = false,byte=false;
	Param* src, *dst;
	this->pc = *(uint16_t*)(&memory[0]);
	while (!end) {
		getInstrDescr(instrDescr);
		instr = instrDescr >> 3;
		byte = (instrDescr & 4) ? false : true;
		switch (instr) {
		case halt:
			end = true;
			break;
		case iret:
			execIret(); break;
		case ret:
			execRet(); break;
		case _int:
			dst = readParam(byte); 
			execInt(dst); break;
		case call:
			dst = readParam(byte);
			execCall(dst); break;
		case jmp: case jeq: case jne: case jgt:
			dst = readParam(byte);
			execJmp(instr,dst); break;
		case push:
			src = readParam(byte);
			execPush(src); break;
		case pop:
			dst = readParam(byte);
			execPop(dst); break;
		case xchg:
			src = readParam(byte);
			dst = readParam(false);
			execXchg(src, dst); break;
		case mov:
			src = readParam(byte);
			dst = readParam(false);
			execMov(src, dst); break;
		case add: case sub: case mul: case _div:
			src = readParam(byte);
			dst = readParam(false);
			execNum(instr, src, dst); break;
		case cmp:
			src = readParam(byte);
			dst = readParam(false);
			execCmp(src, dst); break;
		case _not: case _and: case _or: case _xor:
			src = readParam(byte);
			dst = readParam(false);
			execLogical(instr, src, dst); break;
		case test:
			src = readParam(byte);
			dst = readParam(false);
			execTest(src, dst); break;
		case shl:
			src = readParam(byte);
			dst = readParam(false);
			execShift(0, src, dst);
			break;
		case shr:
			dst = readParam(false);
			src = readParam(byte);
			execShift(1, src, dst);
			break;
		default://nekorektna instrukcija
			putPcPsw();
			psw |= I;
			pc = 0x002;
		}
		if ((psw & I) == 0){
			int req = getInterruptReq();
			if (req!=-1) {
				putPcPsw();
				psw |= I;
				int entry = req * 2;
				pc= *(uint16_t*)(&memory[entry]);
				if (req == 3) {
					terminal = true;
				}
			}

		}
	}
}
void Emulator::putPcPsw() {
	sp -= 2;
	int16_t* stack = (int16_t*)(&memory[sp]);
	*stack = pc;
	sp -= 2;
	stack = (int16_t*)(&memory[sp]);
	*stack = psw;
}

void Emulator::putInterruptReq(int entry)
{
	mutex.lock();
	buffer.push_back(entry);
	mutex.unlock();
}
int Emulator::getInterruptReq() {
	int ret = -1;
	mutex.lock();
	for (size_t i = 0; i < buffer.size(); i++) {
		ret = buffer[i];
		if ((ret == 2 && !(psw & Tr)) || (ret == 3 && !(psw & Tl))) {
			buffer.erase(buffer.begin() + i);
			break;
		}
	}
	mutex.unlock();
	return ret;
}

void Emulator::dataIn(char c)
{
	full.lock();
	mutexMem.lock();
	memory[data_in] = c;
	mutexMem.unlock();
}

inline void Emulator::execRet()
{
	pc = *(int16_t*)(&memory[sp]);
	sp += 2;
} 
void Emulator::execInt(Param*& dst) 
{
	int16_t dstVal = paramVal(dst);
	dstVal = (dstVal % 8) * 2;
	sp -= 2;
	int16_t* stack = (int16_t*)(&memory[sp]);
	*stack = psw;
	pc = *(int16_t*)(&memory[dstVal]);

}

inline void Emulator::execCall(Param*& dst)
{
	int16_t* stack;
	int16_t dstVal = paramVal(dst);
	sp -= 2;
	stack = (int16_t*)(&memory[sp]);
	*stack = pc;
	pc = dstVal;
}

inline void Emulator::execJmp(int type,Param*& dst)
{
	bool condition = false;
	switch (type) {
	case jmp:
		condition = true;
		break;
	case jeq:
		condition = psw & Z;
		break;
	case jne:
		condition = !(psw & Z);
		break;
	case jgt:
		bool n = psw & N;
		bool o = psw & O;
		bool z = psw & Z;
		bool t = n != o ? true : false;
		condition = (!t) && (!z);
	}
	if (condition) {
		int16_t dstVal = paramVal(dst);
		pc = dstVal;
	}
}

inline void Emulator::execPush(Param*& src)
{
	int16_t* stack;
	int16_t srcVal = paramVal(src);
	sp -= 2;
	stack = (int16_t*)(&memory[sp]);
	*stack = srcVal;
}

inline void Emulator::execPop(Param*& dst)
{
	int16_t* m16;
	int8_t* m8;
	int16_t val;
	if (dst->byte) {
		m8 = (int8_t*)(&memory[sp]); sp++;
		val = *m8;
	}
	else {
		m16 = (int16_t*)(&memory[sp]); sp += 2;
		val = *m16;
	}
	intoDst(dst, val);
}

inline void Emulator::execXchg(Param*& src,Param*& dst)
{
	int16_t srcVal = paramVal(src);
	int16_t dstVal = paramVal(dst);

	intoDst(dst, srcVal);
	if (src->opDescr >> 5) {
		intoDst(src, dstVal);
	}
}

inline void Emulator::execMov(Param*& src,Param*& dst)
{
	int16_t srcVal = paramVal(src);
	
	setZ(srcVal); setN(srcVal);
	
	intoDst(dst, srcVal);
}

inline void Emulator::execNum(int instr,Param*& src,Param*& dst)
{
	int16_t srcVal = paramVal(src);
	int16_t dstVal = paramVal(dst);
	int16_t val;

	if (instr == add) {
		val = srcVal + dstVal;
		setZ(val);
		setN(val);
		if ((val > 0 && srcVal < 0 && dstVal < 0) || (val < 0 && srcVal>0 && dstVal > 0)) psw |= O;
		else psw &= ~O;
		if (srcVal < 0 && dstVal < 0) psw |= C;
		else psw &= ~C;
	}
	else if (instr == sub) {
		val = dstVal - srcVal;
		setFlags(val, srcVal, dstVal);
	}
	else {
		if (instr == mul) val = srcVal * dstVal;
		else if (instr == _div) val = dstVal / srcVal;
		setZ(val); setN(val);
	}
	intoDst(dst, val);
	
}

inline void Emulator::execCmp(Param*& src,Param*& dst)
{
	int16_t srcVal = paramVal(src);
	int16_t dstVal = paramVal(dst);
	setFlags(dstVal - srcVal, srcVal, dstVal);
}

inline void Emulator::execLogical(int instr,Param*& src,Param*& dst)
{
	int16_t val;
	int16_t srcVal = paramVal(src), dstVal = paramVal(dst);

	switch (instr) {
	case _not: val = ~srcVal; break;
	case _and: val = dstVal & srcVal; break;
	case _or: val = dstVal | srcVal; break;
	case _xor: val = dstVal ^ srcVal; break;
	}
	
	setZ(val); setN(val);
	intoDst(dst, val);
}

inline void Emulator::execTest(Param*& src,Param*& dst)
{
	int16_t srcVal = paramVal(src), dstVal = paramVal(dst);
	int16_t val = dstVal & srcVal;
	
	setZ(val); setN(val);
}

inline void Emulator::execShift(int instr,Param*& src,Param*& dst)
{
	int16_t srcVal = paramVal(src), dstVal = paramVal(dst);
	int16_t val;

	if (instr == 0) {
		val = dstVal << srcVal;
		if (dst->byte) {
			if (val & (1 << 8)) {
				psw |= C;
			}
			else psw &= ~C;
		}
		else {
			if (val & (1 << (15 - (srcVal - 1)))) {
				psw |= C;
			}
			else psw &= ~C;
		}
	}
	else if (instr == 1) {
		val = dstVal >> srcVal;
		if (dstVal & (1 << (srcVal - 1))) psw |= C;
		else psw &= ~C;
	}

	setZ(val); setN(val);
	intoDst(dst, val);
}

Emulator::Param* Emulator::readParam(bool b)
{
	uint8_t regMask = 0x1E, reg;
	uint8_t opDescr = *(int8_t*)(&memory[pc]);
	uint16_t val=0;
	int8_t addrType = opDescr >> 5;
	pc++;
	switch (addrType) {
	case imm:
		if (b) {
			val = *(int8_t*)(&memory[pc]);
			pc += 1;
		}
		else {
			val = *(int16_t*)(&memory[pc]);
			pc += 2;
		}
		break;
	case regdir: case regind:
		reg = (opDescr & regMask) >> 1;
		if (reg < 6) val = registers[reg];
		else if (reg == 6) val = sp;
		else val = pc;
		if (b && (opDescr & 1)) {
			val >>= 8;
		}
		else if (b && !(opDescr & 1)) {
			val = val & 0xFF;
		}
		break;
	case offset:
		val = *(int16_t*)(&memory[pc]);
		pc += 2;
		reg = (opDescr & regMask) >> 1;
		if (reg < 6) val += registers[reg];
		else if (reg == 6) val += sp;
		else val += pc;
		break;
	case mem:
		val = *(int16_t*)(&memory[pc]);
		pc += 2;
		break;
	default://error
		break;
	}
	return new Param(opDescr, val, b);
}

void Emulator::intoDst(Param*& dst, int16_t val)
{
	int16_t* m16;
	int8_t* m8;
	int addrType = dst->opDescr >> 5;
	if (addrType == regdir) {
		int8_t regMask = 0x1E;
		int8_t reg = (dst->opDescr & regMask) >> 1;
		if (dst->byte && (dst->opDescr & 1)) {
			dst->val = dst->val & 0xFF;
			val = val << 8;
			val = dst->val | val;
		}

		if (reg < 6) registers[reg] = val;
		else if (reg == 6) sp = val;
		else pc = val;
	}
	else {
		mutexMem.lock();
		if (dst->byte) {
			val &= 0xFF;
			int8_t val1 = (int8_t)val;
			m8 = (int8_t*)(&memory[dst->val]);
			*m8 = val1;
		}
		else {
			m16 = (int16_t*)(&memory[dst->val]);
			*m16 = val;
		}
		if (dst->val == 0xFF00 || dst->val==0xFF01) {//dataOut
			cout << val<<" ";
		}
		else if (dst->val == 0xFF10) {//timer_cfg
			timer->setPeriod(val);
		}
		mutexMem.unlock();
	}
}

inline void Emulator::setZ(int16_t val)
{
	if (val == 0) psw |= Z;
	else psw &= ~Z;
}

inline void Emulator::setN(int16_t val)
{
	if (val < 0) psw |= N;
	else psw &= ~N;
}

inline void Emulator::setFlags(int16_t val, int16_t srcVal, int16_t dstVal)
{
	setZ(val); setN(val);
	if ((dstVal > 0 && srcVal < 0 && val < 0) || (dstVal < 0 && srcVal>0 && val > 0)) psw |= O;
	else psw &= ~O;
	if ((dstVal < 0 && srcVal>0) || (dstVal > 0 && srcVal > 0 && dstVal < srcVal) || (dstVal < 0 && srcVal<0 && dstVal>srcVal)) psw |= C;
	else psw &= ~C;	
}

inline int16_t Emulator::paramVal(Param* p)
{
	int8_t addrType = p->opDescr >> 5;
	int16_t ret;
	switch (addrType) {
	case imm: case regdir:
		ret = p->val;
		break;
	case regind: case offset:case mem:
		if (p->byte) ret = *(int8_t*)(&memory[p->val]);
		else ret = *(int16_t*)(&memory[p->val]);
	}
	return ret;
}









