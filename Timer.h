#ifndef TIMER_H_
#define TIMER_H_
#include "Emulator.h"
#include <thread>
class Timer
{
public:
    Timer(Emulator* em, int period);
    void start();
    void run();
    void setEnd();
    void setPeriod(int val);
private:
    Emulator* em;
    bool end;
    int period;
    thread* nit;
};

#endif