#ifndef EMULATOR_H_
#define EMULATOR_H_
#include <stdint.h>
#include <deque>
#include <mutex>

using namespace std;

class Timer;
class Keylistener;
class Emulator
{
	struct Param {
		uint8_t opDescr;
		uint16_t val;
		bool byte;
		Param(uint8_t opDescr, uint16_t val, bool b) {
			this->opDescr = opDescr;
			this->val = val;
			this->byte = b;
		}
	};
public:
	Emulator(char* memory, int start);
	void execute();
	void putInterruptReq(int entry);
	int getInterruptReq();
	void dataIn(char c);
protected:
	inline int16_t paramVal(Param* p);
	inline void getInstrDescr(uint8_t& instr);
	inline void execIret();
	inline void execRet();
	inline void execInt(Param*& dst);
	inline void execCall(Param*& dst);
	inline void execJmp(int type, Param*& dst);
	inline void execPush(Param*& src);
	inline void execPop(Param*& dst);
	inline void execXchg(Param*& src, Param*& dst);
	inline void execMov(Param*& src, Param*& dst);
	inline void execNum(int instr, Param*& src, Param*& dst);
	inline void execCmp(Param*& src, Param*& dst);
	inline void execLogical(int instr, Param*& src, Param*& dst);
	inline void execTest(Param*& src, Param*& dst);
	inline void execShift(int instr, Param*& src, Param*& dst);
	Param* readParam(bool b);
	void intoDst(Param*& dst, int16_t val);
	inline void setZ(int16_t val);
	inline void setN(int16_t val);
	inline void setFlags(int16_t val, int16_t srcVal, int16_t dstVal);
	void putPcPsw();
private:
	char* memory;
	int16_t registers[6];
	int16_t psw;
	uint16_t sp, pc;
	int _main;
	deque<int> buffer;
	mutex mutexMem;
	mutex full;
	mutex mutex;
	bool initialization;
	bool terminal;
	Timer* timer;
	Keylistener* keylistener;
};

#endif
