#ifndef KEYLISTENER_H_
#define KEYLISTENER_H_
#include "Emulator.h"
#include <thread>

class Keylistener
{
public:
	Keylistener(Emulator* em);
	void run();
	void start();
private:
	Emulator* em;
	bool end;
	thread* nit;
};

#endif
